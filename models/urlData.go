package models

import (
	"math/rand"
	"time"
)

type URLData struct {
	ID            int       `json:"id"`
	OriginalURL   string    `json:"originalurl"`
	ShortnerURL   string    `json:"shortnerurl"`
	RandomString  string    `json:"randomstring"`
	RedirectCount int       `json:"redirectcount"`
	CreatedAt     time.Time `json:"createdat"`
}

var ArrURLData map[int]URLData
var urlID = 1
var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func InitializeArrURLData() {
	ArrURLData = make(map[int]URLData)
}

func AddURLData(originalURL string) URLData {
	var urlData URLData

	urlData.ID = urlID
	urlID++

	localhostLink := "localhost:8080/link/"
	rand.Seed(time.Now().UnixNano())
	urlData.RandomString = randSeq(6)
	urlData.ShortnerURL = localhostLink + urlData.RandomString
	urlData.CreatedAt = time.Now()
	urlData.RedirectCount = 0
	urlData.OriginalURL = originalURL

	store(urlData)

	return urlData
}

func GetArrURLData() map[int]URLData {
	return ArrURLData
}

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	randomString := string(b)
	duplicate := checkDuplicateShortURL(randomString)
	if duplicate {
		return randSeq(n)
	}

	return randomString
}

func store(urlData URLData) {
	ArrURLData[urlData.ID] = urlData
}

func checkDuplicateShortURL(randomString string) bool {
	duplicate := false
	for k := range ArrURLData {
		if ArrURLData[k].RandomString == randomString {
			duplicate = !duplicate
			break
		}
	}
	return duplicate
}
