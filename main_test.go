package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"

	u "url-shortener/controller"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/create", u.AddURLData).Methods("POST")
	router.HandleFunc("/link/{shortlink}", u.RedirectLink).Methods("GET")
	router.HandleFunc("/links", u.GetAllLinks).Methods("GET")
	return router
}
func TestDisplayURLData(t *testing.T) {
	request, _ := http.NewRequest("GET", "/links", nil)
	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	assert.Equal(t, 200, response.Code, "OK response is expected")
}
func TestInsertURLData(t *testing.T) {
	u.InitializeArrURLData()
	data := url.Values{}
	data.Add("originalurl", "asdasd.com")
	b := bytes.NewBuffer([]byte(data.Encode()))
	request := httptest.NewRequest("POST", "/create", b)
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	assert.Equal(t, 200, response.Code, "OK response is expected")
}
