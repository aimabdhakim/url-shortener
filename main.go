package main

import (
	"log"
	"net/http"

	u "url-shortener/controller"

	"github.com/gorilla/mux"
)

func main() {
	u.InitializeArrURLData()

	router := mux.NewRouter()

	router.HandleFunc("/create", u.AddURLData).Methods("POST")
	router.HandleFunc("/link/{shortlink}", u.RedirectLink).Methods("GET")
	router.HandleFunc("/links", u.GetAllLinks).Methods("GET")

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		log.Fatalln("There's an error with the server", err)
	}
}
