package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	m "url-shortener/models"

	"github.com/gorilla/mux"
)

var AddURLData = func(writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()
	if err != nil {
		log.Fatalln("There was an error while parsing the form")
	}

	var urlData m.URLData
	originalURL := request.Form.Get("originalurl")
	if originalURL != "" {
		urlData = m.AddURLData(originalURL)
	}

	if urlData != (m.URLData{}) {
		writer.Header().Set("Content-Type", "application/json")
		writer.WriteHeader(http.StatusOK)

		err = json.NewEncoder(writer).Encode(&urlData)
		if err != nil {
			log.Fatalln("There was an error encoding the initialized struct")
		}
	} else {
		writer.Header().Set("Content-Type", "application/json")
		writer.WriteHeader(http.StatusNotFound)
	}

}

var RedirectLink = func(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")

	shortLink := mux.Vars(request)["shortlink"]
	fmt.Println(shortLink)

	arrURLData := m.GetArrURLData()
	fmt.Println(arrURLData)

	for k := range arrURLData {
		fmt.Println(arrURLData[k])
		if arrURLData[k].RandomString == shortLink {
			fmt.Println(arrURLData[k].OriginalURL)
			writer.WriteHeader(http.StatusSeeOther)
			tempUrlData := arrURLData[k]
			tempUrlData.RedirectCount++

			arrURLData[k] = tempUrlData

			err := json.NewEncoder(writer).Encode(arrURLData[k])
			if err != nil {
				log.Fatalln("There was an error encoding the initialized struct")
			}

			http.Redirect(writer, request, arrURLData[k].OriginalURL, http.StatusSeeOther)

			break
		}
	}

	writer.WriteHeader(http.StatusNotFound)
}

var GetAllLinks = func(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)

	arrURLData := m.GetArrURLData()
	fmt.Println(arrURLData)

	err := json.NewEncoder(writer).Encode(&arrURLData)
	if err != nil {
		log.Fatalln("There was an error encoding the initialized struct")
	}
}

func InitializeArrURLData() {
	m.InitializeArrURLData()
}
