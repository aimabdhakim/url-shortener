# URL-Shortener

URL-Shortener is a Golang project for shortening URL. It was using MVC design pattern (https://www.calhoun.io/using-mvc-to-structure-go-web-applications/) because it makes the code tidier and easier to understand.

## Installation

Go to this golang directory, and run:

```bash
go run main.go
```

## Usage

I'm using Postman to use this.

### For add/create

URL: localhost:8080/create

Request: In the tab body, use "x-www-form-urlencoded" with key: originalurl and value: the links

Response: If success, the response will be like this 

{
    "id": 1,
    "originalurl": "facebook.com/test123",
    "shortnerurl": "localhost:8080/link/Wcnlna",
    "randomstring": "Wcnlna",
    "redirectcount": 0,
    "createdat": "2022-11-27T20:59:14.6827392+07:00"
}

### For redirect

URL: localhost:8080/link/<6 random string that we generate earlier>

Request: No need anything, it just need the url

Response: If success, the response will be like this, but it will have 303 response code.

{
    "id": 1,
    "originalurl": "facebook.com/test123",
    "shortnerurl": "localhost:8080/link/vAofLe",
    "randomstring": "vAofLe",
    "redirectcount": 1,
    "createdat": "2022-11-27T21:19:47.3359883+07:00"
}

### For seeing all links

URL: localhost:8080/links

Request: No need anything, it just need the url

Response: If success, the response will be like this

{
    "1": {
        "id": 1,
        "originalurl": "facebook.com/test123",
        "shortnerurl": "localhost:8080/link/vAofLe",
        "randomstring": "vAofLe",
        "redirectcount": 1,
        "createdat": "2022-11-27T21:19:47.3359883+07:00"
    },
    "2": {
        "id": 2,
        "originalurl": "facebook.com/test123",
        "shortnerurl": "localhost:8080/link/pdaslx",
        "randomstring": "pdaslx",
        "redirectcount": 0,
        "createdat": "2022-11-27T21:21:27.2760273+07:00"
    },
    "3": {
        "id": 3,
        "originalurl": "facebook.com/test123",
        "shortnerurl": "localhost:8080/link/qEmzEr",
        "randomstring": "qEmzEr",
        "redirectcount": 0,
        "createdat": "2022-11-27T21:21:27.8593817+07:00"
    },
    "4": {
        "id": 4,
        "originalurl": "facebook.com/test123",
        "shortnerurl": "localhost:8080/link/EAkdLQ",
        "randomstring": "EAkdLQ",
        "redirectcount": 0,
        "createdat": "2022-11-27T21:21:28.3685869+07:00"
    }
}
